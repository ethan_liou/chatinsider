package com.chatinsights.android;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;

import android.media.AudioTrack;
import android.util.Log;

public class PlayThread extends Thread {
	AudioTrack mTrack;
	int mSampleRate;
	private short[] mBuffer;
	DrawView mDrawView;
	DrawView mOverallDrawView;
	String mInputPath;

	public PlayThread(AudioTrack track, int bufSize, int sampleRate,
			String inputPath) {
		super();
		mTrack = track;
		mBuffer = new short[bufSize];
		mSampleRate = sampleRate;
		mInputPath = inputPath;
	}

	public void setView(DrawView drawView) {
		mDrawView = drawView;
		mDrawView.setBuffer(new WeakReference<short[]>(mBuffer));
	}

	public void setOverallView(DrawView drawView) {
		mOverallDrawView = drawView;
		mOverallDrawView.setBuffer(new WeakReference<short[]>(mBuffer));
	}

	public void run() {
		FileInputStream fis = null;
		FileChannel fc = null;
		try {
			File input = new File(mInputPath);
			ByteBuffer myByteBuffer = ByteBuffer.allocate(mBuffer.length*2);
			myByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
			ShortBuffer myShortBuffer = myByteBuffer.asShortBuffer();
			fis = new FileInputStream(input);
			fc = fis.getChannel();

			while (true) {
				int nRet = fc.read(myByteBuffer);
				if(nRet <= 0)break;
				myByteBuffer.flip();
				myShortBuffer.get(mBuffer);
				myShortBuffer.rewind();
				myByteBuffer.rewind();
				
				if (mDrawView != null) {
					mDrawView.mLength = mBuffer.length;
					mDrawView.postInvalidate();
				}				
				mTrack.write(mBuffer, 0, mBuffer.length);
				sleep(1);
			}
		} catch (Exception e) {
			Log.e("Excep", "" + e);
		} finally {
			try {
				fc.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
