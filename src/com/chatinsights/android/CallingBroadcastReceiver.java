package com.chatinsights.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class CallingBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		if(intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)){
			// out
			
		}
		else{
			// in
		}
		
		if(intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)){
			String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
			Log.e("State",""+state);				
			if(state.equals("OFFHOOK")){
				new Thread(new Runnable() {
					@Override
					public void run() {
						context.startService(new Intent(context, CallingRecordService.class));						
					}
				}).start();
			}
			else if(state.equals("IDLE")){
				context.stopService(new Intent(context, CallingRecordService.class));
			}
		}
		
	}
}