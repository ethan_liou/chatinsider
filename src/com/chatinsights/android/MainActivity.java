package com.chatinsights.android;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements View.OnClickListener {
	final String kOutput = "output.wav";
	boolean mIsRecording = false;
	boolean mIsPlaying = false;
	final int kFreq = 44100;
	AudioRecord mRecord;
	int mBufferSize;
	RecordThread mRecordThread;

	AudioTrack mTrack;
	PlayThread mPlayThread;

	
	DrawView mDrawView;

	// MediaRecorder mRecorder;
	// MediaPlayer mPlayer;
	// MediaCodec mCodec;
	// File mCurrentFile;

	private void setRecording(boolean isRecording) {
		mIsRecording = isRecording;
		if (!mIsRecording) {
			findViewById(R.id.prev).setVisibility(View.VISIBLE);
			findViewById(R.id.next).setVisibility(View.VISIBLE);
			findViewById(R.id.play).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.prev).setVisibility(View.INVISIBLE);
			findViewById(R.id.next).setVisibility(View.INVISIBLE);
			findViewById(R.id.play).setVisibility(View.INVISIBLE);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);
		setRecording(false);
		mDrawView = (DrawView) findViewById(R.id.main_dv);
		Button prevBtn = (Button) findViewById(R.id.prev);
		prevBtn.setOnClickListener(this);
		Button nextBtn = (Button) findViewById(R.id.next);
		nextBtn.setOnClickListener(this);
		Button recordBtn = (Button) findViewById(R.id.record);
		recordBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!mIsRecording) {
					mBufferSize = AudioRecord.getMinBufferSize(kFreq,
							AudioFormat.CHANNEL_IN_MONO,
							AudioFormat.ENCODING_PCM_16BIT);
					mRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
							kFreq, AudioFormat.CHANNEL_IN_MONO,
							AudioFormat.ENCODING_PCM_16BIT, mBufferSize);
					mRecord.startRecording();
					String path = null;
					{
						File f = new File(getFilesDir(),kOutput);
						path = f.getAbsolutePath();
						f.delete();
					}
					mRecordThread = new RecordThread(mRecord, mBufferSize,
							kFreq,path);
					mRecordThread.setView(mDrawView);
					mRecordThread.start();
					setRecording(true);
				} else {
					mRecord.stop();
					mRecord.release();
					mRecord = null;
					mRecordThread.interrupt();
					try {
						mRecordThread.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					setRecording(false);
					Toast.makeText(MainActivity.this, "Finish",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		Button playBtn = (Button) findViewById(R.id.play);
		playBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!mIsPlaying){
					File f = new File(getFilesDir(),kOutput);
					if(f.exists()){
						Log.e("Length",""+f.length());
						mBufferSize = AudioTrack.getMinBufferSize(kFreq,
								AudioFormat.CHANNEL_OUT_MONO,
								AudioFormat.ENCODING_PCM_16BIT);
						mTrack = new AudioTrack(MediaRecorder.AudioSource.MIC,
								kFreq, AudioFormat.CHANNEL_OUT_MONO,
								AudioFormat.ENCODING_PCM_16BIT, mBufferSize,AudioTrack.MODE_STREAM);
						mTrack.play();
						String path = f.getAbsolutePath();
						mPlayThread = new PlayThread(mTrack, mBufferSize,kFreq,path);
						mPlayThread.setView(mDrawView);
						mPlayThread.start();
						mIsPlaying = true;
					}
					else{
						Toast.makeText(MainActivity.this, "You should record first", Toast.LENGTH_SHORT).show();
					}
				}
				else{
					mTrack.stop();
					mTrack.release();
					mTrack = null;
					mPlayThread.interrupt();
					try {
						mPlayThread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					mIsPlaying = false;
				}					
			}
		});
		// recordBtn.setOnClickListener(new View.OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// if (!mIsRecording) {
		// try {
		// mBufferSize = AudioRecord.getMinBufferSize(kFreq,
		// AudioFormat.CHANNEL_IN_MONO,
		// AudioFormat.ENCODING_PCM_16BIT);
		// mRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, kFreq,
		// AudioFormat.CHANNEL_IN_MONO,
		// AudioFormat.ENCODING_PCM_16BIT, mBufferSize);
		// } catch (IllegalStateException e) {
		// e.printStackTrace();
		// }
		// mRecord.startRecording();
		// mRecordThread = new RecordThread(mRecord, mBufferSize, kFreq);
		// mRecordThread.setView(mDrawView);
		// mRecordThread.start();
		// mIsRecording = true;
		// }
		// else{
		// try {
		// mRecordThread.interrupt();
		// mRecordThread.join();
		// mRecordThread = null;
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// mRecord.stop();
		// mRecord.release();
		// mRecord = null;
		// mIsRecording = false;
		// }
		// }
		// });
		// setContentView(R.layout.activity_main);
		// Button btn = (Button) findViewById(R.id.recordBtn);
		// btn.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Intent intent = new Intent(MainActivity.this,
		// CallingRecordService.class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		// if (mIsRecording) {
		// stopService(intent);
		// mIsRecording = false;
		// } else {
		// startService(intent);
		// mIsRecording = true;
		// }
		// }
		// });
		// ListView lv = (ListView) findViewById(R.id.recordList);
		// final String[] records = getFilesDir().list();
		// lv.setAdapter(new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, records));
		// lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// if (mIsRecording) {
		// mPlayer.stop();
		// mIsRecording = false;
		// } else {
		// File f = new File(getFilesDir(), records[position]);
		// mPlayer = new MediaPlayer();
		// try {
		// mPlayer.setDataSource(f.getAbsolutePath());
		// mPlayer.prepare();
		// mPlayer.start();
		// mIsRecording = true;
		// } catch (IllegalArgumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (SecurityException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IllegalStateException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// }
		// });
	}

	@Override
	public void onClick(View v) {
		// final int step = 100;
		// int dir = v.getId() == R.id.prev ? -1 : 1;
		// mDrawView.mOffset += (dir * step);
		// if(mDrawView.mOffset < 0)mDrawView.mOffset = 0;
		// if(mDrawView.mOffset + mDrawView.mLength >
		// mDrawView.mBuffer.get().length)mDrawView.mOffset =
		// mDrawView.mBuffer.get().length - mDrawView.mLength;
		// mDrawView.invalidate();
	}
}
