package com.chatinsights.android;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.channels.FileChannel;

import android.media.AudioRecord;
import android.util.Log;

public class RecordThread extends Thread {
	AudioRecord mRecord;
	int mSampleRate;
	private short[] mBuffer;
	DrawView mDrawView;
	DrawView mOverallDrawView;
	String mOutputPath;

	public RecordThread(AudioRecord record, int bufSize, int sampleRate,
			String outputPath) {
		super();
		mRecord = record;
		mBuffer = new short[bufSize];
		mSampleRate = sampleRate;
		mOutputPath = outputPath;
	}

	public void setView(DrawView drawView) {
		mDrawView = drawView;
		mDrawView.setBuffer(new WeakReference<short[]>(mBuffer));
	}

	public void setOverallView(DrawView drawView) {
		mOverallDrawView = drawView;
		mOverallDrawView.setBuffer(new WeakReference<short[]>(mBuffer));
	}

	public void run() {
		FileChannel fc = null;
		FileOutputStream fos = null;
		try {
			File output = new File(mOutputPath);
			ByteBuffer myByteBuffer = ByteBuffer.allocate(mBuffer.length * 2);
			myByteBuffer.order(ByteOrder.LITTLE_ENDIAN);

			ShortBuffer myShortBuffer = myByteBuffer.asShortBuffer();
			fos = new FileOutputStream(output);
			fc = fos.getChannel();
			while (true) {
				int nRead = 0;
				while(nRead < mBuffer.length){
					int nByte = mRecord.read(mBuffer, nRead, mBuffer.length - nRead);	
					if(nByte < 0)break;
					nRead += nByte;
				}
				if (mDrawView != null) {
					mDrawView.mLength = mBuffer.length;
					mDrawView.postInvalidate();
				}
				myShortBuffer.put(mBuffer);
				int nRet = fc.write(myByteBuffer);
				Log.e("nRet",""+nRet);
				myShortBuffer.rewind();
				myByteBuffer.rewind();
				sleep(1);
			}
		} catch (Exception e) {
			Log.e("Excep", "" + e);
		} finally {
			try {
				fos.close();
				fc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
