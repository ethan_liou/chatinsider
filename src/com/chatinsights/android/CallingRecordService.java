package com.chatinsights.android;

import android.app.Service;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.util.Log;

public class CallingRecordService extends Service{
	final int kFreq = 44100;
	int mBufferSize=0;
	AudioRecord mRecord;
	RecordThread mRecordThread;
		
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	} 
	
	void InitRecorder(){
		try {
			mBufferSize = AudioRecord.getMinBufferSize(kFreq, AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT);
			mRecord = new AudioRecord(MediaRecorder.AudioSource.VOICE_DOWNLINK,kFreq,AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT,mBufferSize);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.e("Service","OnStart");
		InitRecorder();
		mRecord.startRecording();
		mRecordThread = new RecordThread(mRecord, mBufferSize,kFreq,null);
		mRecordThread.start();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e("Service","OnStop");
		mRecord.stop();
		mRecord.release();
		mRecord = null;
		mRecordThread.interrupt();
		try {
			mRecordThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
