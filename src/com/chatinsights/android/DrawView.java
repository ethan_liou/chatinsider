package com.chatinsights.android;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class DrawView extends View {
	WeakReference<short[]> mBuffer;
	Paint mLinePaint;
	Paint mPointPaint;
	int mLength;
	Path mPath;
	
	public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setBackgroundColor(Color.BLACK);
		mLinePaint = new Paint();
		mLinePaint.setStyle(Style.STROKE);
		mLinePaint.setStrokeWidth(1.0f);
		mLinePaint.setColor(Color.GREEN);
		mPointPaint = new Paint();
		mPointPaint.setColor(Color.RED);
		mPointPaint.setStrokeWidth(5.0f);
		mPath = new Path();
	}

	public DrawView(Context context, AttributeSet attrs) {
		this(context, attrs,0);
	}

	public DrawView(Context context) {
		this(context,null);
	}

	public void setBuffer(WeakReference<short[]> buffer){
		mBuffer = buffer;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if(mBuffer != null && mBuffer.get() != null){
			// mBytes -> Width 
			int width = canvas.getWidth();
			int height = canvas.getHeight();
			int count = (int) Math.floor(1.0f * mLength / width);
			if(count == 0)
				return;
			mPath.reset();
			mPath.moveTo(0, height / 2);
			// sum * max_height / 65536 * count = value
			int lastY = 0;
			int lastDiff = 0;
			for(int i = 0 ; i < width ; i++){
				int sum = 0;
				for(int j = 0 ; j < count && i * count + j < mLength; j ++){
					sum += mBuffer.get()[i*count+j];
				}
				int y = sum * height / 32768 / count + height / 2;
				int diff = y - lastY;
				if(diff >= 0 && lastDiff < 0) // max
					canvas.drawPoint(i, y, mPointPaint);
				lastDiff = diff;
				lastY = y;
				mPath.lineTo(i, y);
			}
			canvas.drawPath(mPath, mLinePaint);
		}
	}
}
